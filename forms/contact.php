<?php
  /**
  * Requires the "PHP Email Form" library
  * The "PHP Email Form" library is available only in the pro version of the template
  * The library should be uploaded to: vendor/php-email-form/php-email-form.php
  * For more info and help: https://bootstrapmade.com/php-email-form/
  */

  if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptcha_response'])) {
    try {
    
      $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
      $recaptcha_secret = '6Lckfv4UAAAAALO6MlRP-xYPGwt9yAuzq7QnvBy0';
      $recaptcha_response = $_POST['recaptcha_response'];

      // Make and decode POST request:
      $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
      $recaptcha = json_decode($recaptcha);
      // Take action based on the score returned:
      if(isset($recaptcha->score)){
      if ($recaptcha->score >= 0.5) {
          // Verified - send email
          send_email();
          echo "Thanks, we will contact you soon.";
      } else {
          // Not verified - show form error
          echo "Someing went wrong, Please reload page and try again";

        }
      } else {
        echo "Someing went wrong, Please reload page and try again.";

      }
    } catch (Throwable $th) {
      echo "Already Sent.";

    }
          

  }

  function send_email(){
    $msg = "<H3>Contact form details</H3>";
    $msg .= "Name :: ".$_POST['name']."<br/>";
    $msg .= "Email :: ".$_POST['email']."<br/>";
    $msg .= "Subject :: ".$_POST['subject']."<br/>";
    $msg .= "Message :: ".$_POST['message']."<br/>";

    sendEmail("info@ssfmcg.com","info@ssfmcg.com","Contact Form SSFMCG.com",$msg);
  }

  function sendEmail($to,$from,$sub,$msg){

    $curl = curl_init();
    $to_all = '';
    if(gettype($to) == "array"){
       foreach ($to as $key => $value) {
          $to_all .= '{"email": "'.$value.'"}';
          if($key+1 < sizeof($to)){
            $to_all .=',';
          }
       }
        
    } else {
        $to_all = '{"email": "'.$to.'"}';
    }

    $value = ["type"=> "text/html","value"=>$msg];
    $value_json = json_encode($value, true);
    $data = '{"personalizations": [{"to": ['.$to_all.']}],"from": {"email": "'.$from.'"},"subject": "'.$sub.'","content": ['.$value_json.']}';


    // pr($data);die();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $data,
      CURLOPT_HTTPHEADER => array(
        "authorization: Bearer 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",
        "cache-control: no-cache",
        "content-type: application/json",
        "postman-token: 07c3e745-b396-88c1-7e26-5121e1eba03d"
      ),
    ));
    
    $response = curl_exec($curl);
    $err = curl_error($curl);
    
    curl_close($curl);
    
    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
 }













  
?>
